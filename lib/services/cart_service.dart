import 'dart:async';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/storage/mock_store.dart';

abstract class CartService {
  Stream<int> get cartCountStreamFromCartInDatabase;

  Stream<Map<String, int>> get cartItemsMapStreamFromCartInDatabase;

  Future addToCartInDatabase(Product p, int qty);

  Future<void> removeFromCart(String p, int qty);
}

/// Example implementation that *does not persist beyond the session*
class CartServiceTemporary implements CartService {
  final AppStore store;

  CartServiceTemporary(this.store);

  @override
  Future addToCartInDatabase(Product p, int qty) async {
    // Update existing cart in AppStore
    store.cart // gets the existing cart in AppStore
        ..totalCartItems += qty // update cart properties
        ..items[p.title] = _currentCountForProduct(p) + qty;
//    store.cartBroadcastController.add(_cart);  // Then add that updated cart to the corresponding stream again
//    store.cart = store.cart; // used to fake reactivity | New Cart added in store.cartStreamController./sink
    store.cartUpdated();
  }

  // above function updates the CartCount in database
  // below function fetches same CartCount from database
  // above function adds data to same StreamController./sink whose stream below function exposes
  @override
  Stream<int> get cartCountStreamFromCartInDatabase => // if you listen to this, you are listening from Firestore
     store.cartBroadcastCt.stream.map((snapshot) => store.cart.totalCartItems);
   // AppCartIcon listen to this | here the Service fetches whole cart from database & streams totalCartItems from it

  @override
  Stream<Map<String, int>> get cartItemsMapStreamFromCartInDatabase =>
      store.cartBroadcastCt.stream.map((Cart dartData) => store.cart.items);

  @override
  Future removeFromCart(String p, int qty) async {
    store.cart
      ..totalCartItems -= qty
      ..items.remove(p);
//    store.cart = store.cart; // used to fake reactivity
//    store.cartStreamController.add(store.cart);
   store.cartUpdated();
  }

  int _currentCountForProduct(Product p) {
    if (!store.cart.items.containsKey(p.title)) return 0;
    return store.cart.items[p.title];
  }
}

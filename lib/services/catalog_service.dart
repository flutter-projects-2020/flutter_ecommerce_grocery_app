import 'dart:async';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/storage/mock_store.dart';
import 'package:e_commerce/utils/generate_cart_data.dart';

abstract class CatalogService {
  Stream<List<Product>> pLStream();
  Stream<List<Product>> pLStreamFor(ProductCategory category);
  Future<void> addNewProduct(Product product);
  Future<void> updateProduct(Product product);
}

/// Example implementation that *does not persist beyond the session*
class CatalogServiceTemporary implements CatalogService {
  final AppStore store;

  CatalogServiceTemporary(this.store);

  @override
  Future addNewProduct(Product product) async {
    store.catalog.availableProducts.add(product);
  }

  @override
  Stream<List<Product>> pLStreamFor(ProductCategory category) {
    return store.catalogBroadcastCt
        .stream.map( // map,ie,converts Stream<Catalog> to a Stream<Catalog.availableProducts>
            (Catalog catalogSnapshot) =>
        catalogSnapshot.availableProducts.where((Product p) => p.category == category).toList()
    );
  }
  // returns diff stream(to listen-to) on basis of category
  @override
  Stream<List<Product>> pLStream() { // stream the available products in Catalog, not whole Catalog
    return store.catalogBroadcastCt.stream.map((Catalog snapshot) { // convert function
      return store.catalog.availableProducts;
    });
  }

  @override
  Future updateProduct(Product product) async {
    var orinalProduct =
        store.catalog.availableProducts.firstWhere((Product p) => p.title == product.title);
    store.catalog.availableProducts.remove(orinalProduct);
    store.catalog.availableProducts.add(product);
  }
}

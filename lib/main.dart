/*
 * Copyright 2018 Eric Windmill. All rights reserved.
 * Use of this source code is governed by the MIT license that can be found in the LICENSE file.
 */

import 'package:e_commerce/app.dart';
import 'package:e_commerce/blocs/app_state.dart';
import 'package:e_commerce/blocs/cart_bloc.dart';
import 'package:e_commerce/blocs/catalog_bloc.dart';
import 'package:e_commerce/blocs/user_bloc.dart';
import 'package:e_commerce/services/cart_service.dart';
import 'package:e_commerce/services/catalog_service.dart';
import 'package:e_commerce/services/user_service.dart';
import 'package:e_commerce/storage/mock_store.dart';
import 'package:flutter/material.dart';

/// In this app, we initialize the Firestore, AppBloc, and ServiceProvider
/// right from the start, then inject them into the app.
Future<void> main() async {
  /// Wrap the app in the AppBloc
  /// An inherited widget which keeps track of
  /// the state of the app, including the
  /// active page
  runApp(LoginApp());
}

class LoginApp extends StatefulWidget {
  @override
  _LoginAppState createState() => _LoginAppState();
}

class _LoginAppState extends State<LoginApp> {
  String _userId;
  bool isUserLoggedIn;
  bool checkUserLogin() {
    //todo: logic to check UserLogin
    return true;
  }
  // to be executed from inside App
  void userLogout() {
    // todo: log out user only if connected to internet
    setState(() {
      isUserLoggedIn = false;
    });
  }
  // to be executed from inside login Page
  void userLoginIn() {
    // todo: logic runs only if connected to internet
    // todo: logic to make user signin or signup
    // if(email exists in CustomersRecord database) signin
    // if(email not exists in CustomersRecord database) prompt & say to signup
    _userId = ''; // set on successful login
    setState(() {
      isUserLoggedIn = true;
    });
  }
  @override
  void initState() {
    super.initState();
    isUserLoggedIn = checkUserLogin();
  }
  //todo: if user logged in then build the App otherwise build a loginPage
  @override
  Widget build(BuildContext context) {
    if (!isUserLoggedIn)
      return LoginPage(
        userLoginFn: userLoginIn,
      );
    else
      return App(
        //AppBuilder | builds an InheritedWidget tree, with logic & data containing instances in top of child, where the bloc-instance can be accessed
        userLogoutFn: userLogout,
        userId: _userId,
      );
  }
}

class LoginPage extends StatefulWidget {
  final Function userLoginFn;
  LoginPage({Key key, @required this.userLoginFn}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Function userLoginFn;
  @override
  void initState() {
    super.initState();
    userLoginFn = widget.userLoginFn;
  }
  @override
  Widget build(BuildContext context) {
    return Container(); // todo: Make a login Form here, that should execute UserLoginFn
  }
}

//builds an App with blocs & its state injected in it
class App extends StatefulWidget {
  final String userId;
  final Function userLogoutFn;
  const App({
    Key key,
    @required this.userId, // todo: AppStore should be initialized on specific userId
    @required this.userLogoutFn,
  }) : super(key: key);

  @override
  AppState createState() => AppState();
}

class AppState extends State<App> {
  // User level state should be here
  // todo: appBlocs should only load when a user logs in
  Blocs appBlocs;
  Function userLogoutFn;
  //todo: functions for userLogin & userLogOut should be in this AppState
  //todo: on UserLogin the appBlocs should load
  //todo: on UserReLogin the appBlocs should get reassigned & setState called
  @override
  void initState() {
    super.initState();
    appBlocs = fetchBlocsFor(widget.userId); // todo: change appBlocs when user switches and call setState
    userLogoutFn = widget.userLogoutFn;
  }
  Blocs fetchBlocsFor(String userId) {
    // on the basis of user loggedIn or GuestUser load appBlocs
    var store = AppStore(userId: userId);
    var catalogService = CatalogServiceTemporary(store);
    var cartService = CartServiceTemporary(store);
    var userService = MockUserService(store);

    /// Starting here, everything is used regardless of dependencies
    //all business logic components used in the app
    var blocs = Blocs(
      cartBloc: CartBloc(cartService), // CartBloc(CartService(AppStore()))
      catalogBloc: CatalogBloc(catalogService),
      userBloc: UserBloc(userService),
    );

    return blocs;
  }
  @override
  Widget build(BuildContext context) {
    return BlocsInjector( // injecting this state only to access any function in this state
      // An InheritedWidget, it and hence its members can be accessed down the tree
      appState: this, // <- can be accessed down in tree, then that context will be registered by the framework, to be rebuilt when the InheritedWidget changes/rebuilds && its update
      blocs: appBlocs, // <- ----- "" ---------
      child: ECommerceApp(), //ActualApp,
    );
  }

  @override
  void dispose() {
    appBlocs
    ..cartBloc.close()
    ..catalogBloc.close()
    ..userBloc.close();
    super.dispose();
  }
}
/* NO NEED OF AN APP-STATE, IF B.Lo.Cs used, all logic & data should be accessed from them
class AppState extends State<App> {
  // Blocs get blocs => widget.blocs;

  @override
  Widget build(BuildContext context) {
    return AppAssets( // An InheritedWidget, it and hence its members can be accessed down the tree
//      appState: this, // <- can be accessed down in tree, then that context will be registered by the framework, to be rebuilt when the InheritedWidget changes/rebuilds && its update
      appBlocs: widget.blocs, // <- ----- "" ---------
      child: widget.child, // ECommerceApp
    );
  }

  void dispose() {
    super.dispose();
  }

//  // 'LIFTING STATE UP' REGION:
//  int cartCount = 0;
//  void updateCartTotal(int count) {
//    setState(() => cartCount += count);
//  }
}
*/

import 'package:flutter/material.dart';
// Transitions handled by PageRoute | Route-Classes -> MaterialPageRoute : PageRoute : ModalRoute : TransitionRoute
// Custom PageTransition, involves 2 pages, one that is coming into view, other that is leaving view
// Pages are widget, that means they can be animated too
// Pages have default transition, differ by platform | Animation for that transition is generated by MaterialPageRoute or CupertinoPageRoute

class FadeInSlideOutRoute<T> extends MaterialPageRoute<T> {
  FadeInSlideOutRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  // MaterialPageRoute already has transitions/animations built in its class, we don't need to build animation, we can override its buildTransitions
  // AnimationController & Tween, are all written in the superclass
  @override
  Widget buildTransitions(BuildContext context, // takes two animations as argument
      Animation<double> animation,              // 1. for itself as it exits
      Animation<double> secondaryAnimation,     // 2. to coordinate with the route that's replacing it
      Widget child) {
    if (settings.isInitialRoute) return child;
    if (animation.status == AnimationStatus.reverse)
      return super.buildTransitions(context, animation, secondaryAnimation, child);

    // By overiding, we have to only return a Page wrapped in an AnimatedWidget
    // Return a built-in Flutter transition pass it in the animation built by superclass
    return FadeTransition(opacity: animation, child: child);
    // Other built-in transitions: Slide, Size, Scale, Rotation, Align
  }
}

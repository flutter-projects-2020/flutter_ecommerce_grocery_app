import 'dart:math' as math;

import 'package:e_commerce/models/cart.dart';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/storage/mock_store.dart';
import 'package:e_commerce/utils/product_seeds.dart';
import 'package:e_commerce/models/catalog.dart';

/// Search through the [pdCtgToPdTitle] Map and find the category
/// who's value list contains this product.
/// 'NOT EFFICIENT', but fine considering the small dataset.
ProductCategory getCategoryForProduct(String pN) => pdCtgToPdTitle.entries
    .firstWhere(
        (MapEntry<ProductCategory, List<String>> entry) => entry.value.contains(pN))
    .key;

/// Generate random cost for product roughly between 1 and 3 USD.
var _random = math.Random();

double determineCost() {
  var cost = (_random.nextDouble() * 3.3).clamp(0.7, 3.3).toStringAsFixed(2);
  return double.parse(cost);
}

Product createProduct(String productName) {
  return Product(
      title: productName,
      imageTitle: fakeProductsToImageTitle[productName],
      category: getCategoryForProduct(productName),
      cost: determineCost());
}
class Catalog {
  List<Product> availableProducts = [];
}
// returnPopulatedCatalogInstance
Catalog buildInitialCatalog() {
  var catalog = Catalog();
  fakeProductsToImageTitle.forEach((String productName, _) {
    catalog.availableProducts.add(createProduct(productName));
  });

  return catalog;
}

Cart buildInitialCart() {
  return Cart()
    ..items = {}
    ..totalCartItems = 0
    ..totalCost = 0.00;
}

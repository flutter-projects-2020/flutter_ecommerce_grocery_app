import 'dart:async';
import 'package:e_commerce/models/catalog.dart';
import 'package:e_commerce/models/user.dart';
import 'package:e_commerce/utils/generate_cart_data.dart';
import 'package:flutter/cupertino.dart';

/// Mocks a reactive database service, like Firestore:
/// updates a datasource that *isn't* persistent,
/// then notifies listeners.
class Cart {
  Map<String, int> items;
  double totalCost;
  int totalCartItems;
}
class AppStore {
  String userId; // todo: make use of this userId to initialize right AppStore & the right User
  Cart _cart;
  Cart get cart => _cart;
//  set cart(Cart c) { //  updated cart comes here in Firestore, can save it here
//    _cart = c;                   // UpdatedCart assigned to existing cart
//    cartStreamController.add(c); //on _addToCart a UpdatedCart added to this stream
//  }
  final cartBroadcastCt = StreamController<Cart>.broadcast();
  // things to do when cart gets updated by user-bloc-service
  void cartUpdated() {          // Widget <- Bloc <- Service <- Database
    cartBroadcastCt.add(_cart); // add the AppStore.cart again in AppStore.cartStreamController so that listening service-bloc-user gets the right info
  }
  Catalog _catalog;
  ECommerceUser _eCommerceUser;
  final catalogBroadcastCt = StreamController<Catalog>.broadcast();
  final eCommerceUserBroadcastCt = StreamController<ECommerceUser>.broadcast();
  Catalog get catalog => _catalog;
  set catalog(Catalog updatedCatalog) {
    _catalog = updatedCatalog;
    catalogBroadcastCt.add(updatedCatalog);
  }

  ECommerceUser get eCommerceUser => _eCommerceUser;
  set eCommerceUser(ECommerceUser c) {
    _eCommerceUser = c;
    eCommerceUserBroadcastCt.add(c);
  }
  AppStore({@required this.userId}) {
    // initialize app fake data
    _cart = buildInitialCart(); // todo: cart should be build a/q to user id | buildCartAccordingToUserRecordInDatabase
    _catalog = buildInitialCatalog(); // todo: catalog remains the same for each user
    _eCommerceUser = ECommerceUser(name: "Satya Prakash", contact: "satya.tryme@gmail.com", userProducts: []);  // todo: right user on basis of userId should be fetched

    // emit initial events, notifying the UI
    Future.delayed(Duration(milliseconds: 500), () {
      catalogBroadcastCt.add(_catalog);
      cartBroadcastCt.add(_cart); // initial Cart added to the Stream
      eCommerceUserBroadcastCt.add(_eCommerceUser);
    });
  }
}

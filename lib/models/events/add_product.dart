import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/models/user.dart';

class NewUserProductEvent {
  final NewProduct product;

  NewUserProductEvent(this.product);
}

class UpdateUserEvent {
  ECommerceUser user;

  UpdateUserEvent(this.user);
}


class RouteChangeEvent {
  final String route;

  RouteChangeEvent(this.route);
}

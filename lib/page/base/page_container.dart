/*
 * Copyright 2018 Eric Windmill. All rights reserved.
 * Use of this source code is governed by the MIT license that can be found in the LICENSE file.
 */

import 'package:e_commerce/menu/app_menu_drawer.dart';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/page/add_product_form.dart';
import 'package:e_commerce/page/base/page_background_image.dart';
import 'package:e_commerce/page/base/page_base.dart';
import 'package:e_commerce/page/cart_page.dart';
import 'package:e_commerce/page/catalog_page.dart';
import 'package:e_commerce/page/user_settings_page.dart';
import 'package:e_commerce/utils/product_seeds.dart';
import 'package:e_commerce/utils/styles.dart';
import 'package:flutter/material.dart';

// for pageTitle in different pages
enum PageType { Catalog, Cart, Settings, ProductDetail, AddProductForm }
// 3 different types of pages using a method known as Code-Reuse <^_^>
//A StatelessWidget that inherits its build method from PageBase
class Page extends PageBase {
  final PageType pageType;

  Page({Key key, @required this.pageType}) : super(key: key);

  @override
  Widget get menuDrawer => AppMenu();

  @override
  String get pageTitle {
    switch (pageType) {
      case PageType.Cart:
        return "My Cart";
      case PageType.Settings:
        return "My Settings";
        break;
      case PageType.AddProductForm:
        return "Add Product";
      case PageType.Catalog:
      default:
        return "The Grocery Market";
    }
  }

  @override
  Widget get body {
    var page;
    switch (pageType) {
      case PageType.Cart:
        page = CartPage();
        break;
      case PageType.Settings:
        page = UserSettingsPage();
        break;
      case PageType.Catalog:
      default:
        page = CatalogPage();
    }
    return Padding(
      padding: EdgeInsets.all(Spacing.matGridUnit()), // and I used percentage of screen here
      child: page,
    );
  }

  @override
  Color get scaffoldBackgroundColor => Color(0xFFF7F7F7); //AppColors.background
}
/*-------------------------------------------------------------------------------------------------------------------*/
//A StatelessWidget that inherits its build method from PageBase
class ProductDetailPage extends PageBase {
  final Product product;

  ProductDetailPage({@required this.product});

//  @override
//  Widget get body => ProductDetailPage(product: product);

  @override
  Widget get background => BackgroundImage(
        key: PageStorageKey(product),
        imageTitle: _getImageForCategory,
      );

  ImageTitle get _getImageForCategory => categoriesToImageTitle[product.category];
}
/*------------------------------------------------------------------------------------------------------------------*/
//A StatelessWidget that inherits its build method from PageBase
class AddNewProductPage extends PageBase {
  @override
  Widget get body => AddProductForm();

  @override
  String get pageTitle => "Add Product";

}

/*
 * Copyright 2018 Eric Windmill. All rights reserved.
 * Use of this source code is governed by the MIT license that can be found in the LICENSE file.
 */

import 'package:e_commerce/menu/app_menu_drawer.dart';
import 'package:e_commerce/widget/appbar_cart_icon.dart';
import 'package:flutter/material.dart';

// Can't instantiate this class only can extend it
// facilitates code reuse between different types of pages
// all pages contain AppBar, CartIcon, Menu, with some changed configuration
abstract class PageBase extends StatelessWidget {
  Widget get body => Container();

  String get pageTitle => "";

  Widget get menuDrawer => null;

  Widget get background => Container();

  Color get scaffoldBackgroundColor => Colors.transparent;

  const PageBase({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size window = MediaQuery.of(context).size;
    final ThemeData theme = Theme.of(context);
    return Stack(
      children: <Widget>[
        Container( // a screen-sized page
          height: window.height,
          width: window.width,
          color: theme.backgroundColor,
        ),
        background, // can put an image at this spot, ColouredContainer-Background-TransparentScaffold
        Scaffold( // this will also expand to fill the device-screen,
          backgroundColor: scaffoldBackgroundColor,
          appBar: AppBar(
            brightness: Brightness.light,
            backgroundColor: Colors.transparent, // that's why there is a ColouredContainer below
            elevation: 0.0,
            title: Text(pageTitle),
            textTheme: theme.primaryTextTheme,
            actions: <Widget>[
              AppBarCartIcon(),
            ],
          ),
          drawer: menuDrawer,
          body: body,
        ),
      ],
    );
  }
}

import 'package:e_commerce/app.dart';
import 'package:e_commerce/blocs/app_state.dart';
import 'package:e_commerce/blocs/user_bloc.dart';
import 'package:e_commerce/models/user.dart';
import 'package:flutter/material.dart';

class AppMenu extends StatefulWidget {
  const AppMenu({Key key}): super(key: key);
  @override
  AppMenuState createState() => AppMenuState();
}
// RouteAware interface for objects that are aware of their current [Route].
//This is used with [RouteObserver] to make a widget aware of changes to the [Navigator]'s session history
class AppMenuState extends State<AppMenu> //with RouteAware
{ // each time you open this Menu, a new State is instantiated
  String _activeRouteBelowMenu = "/";
//  int counter = 0; // need to use Provider/Another-class-as-Inherited.asset to preserve-State
  /*@override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // The given [BuildContext] will be rebuilt if the state of the route changes (specifically, if [isCurrent] or [canPop] change value).
    routeObserver.subscribe(this, ModalRoute.of(context)); // ModalRoute.of(context) returns the route above this-route(=here this AppMenu), initially the initial route
    print('From AppMenuState.didChangeDependencies: ${ModalRoute.of(context).settings.name}');
    // after subscribing to a RouteObserver, you start listening to it & it starts to send events, any route pushed above the ModalRoute.of(context) will be informed to this RouteAware-State object by didPush
    // when this route is popped off didPop will be called

  }
  @override
  void didPush() {
    print('From AppMenuState.didPush: ${ModalRoute.of(context).settings.name}');
    _activeRouteBelowMenu = ModalRoute.of(context).settings.name; // the route/screen below this AppMenu
  }*/

   void _navigate(String route)  {
    // menu should close when a menu item is selected
     if (route == ModalRoute.of(context).settings.name) {
       Navigator.pop(context); // gets the name of route below this menu, if that same route is getting pushed again, menu gets popped only and not pushes the same screen again
       return;
     }
      /*setState(() {
        _activeRouteBelowMenu = route; // before this menu gets popped off, gets rebuild to highlight coming route
      });*/
       Navigator.popAndPushNamed(context, route);
      // this expression will have a value as soon as this process finishes
    // can get a value here back from next route
  }

  @override
  Widget build(BuildContext context) {
    final UserBloc _bloc = BlocsInjector.of(context).blocs.userBloc;
    //    _activeRouteBelowMenu  ??= "/"; // In case of RouteObserver-RouteAware
    _activeRouteBelowMenu = ModalRoute.of(context).settings.name ?? ""; // It seems I don't need any RouteObserver & RouteAware, if I only need to know which page is below this Menu :\
    // here menu needs to show what route is below it
    return Drawer( // houses the Menu
      // The Menu
      child: ListView( // layout-widget | arranges its children in a scrollable-container
        children: <Widget>[ // arranges its children vertically by default
          StreamBuilder(
            initialData: ECommerceUser(name: "", contact: ""),
            stream: _bloc.user,
            builder: (BuildContext context, AsyncSnapshot<ECommerceUser> s) =>
                UserAccountsDrawerHeader(
                  currentAccountPicture: CircleAvatar( // crops an image to circle
                    backgroundImage:
                        AssetImage("assets/images/apple-in-hand.jpg"),
                  ),
                  accountEmail: Text(s.data.contact),
                  accountName: Text(s.data.name),
                  onDetailsPressed: () {
                    Navigator.pushNamed(
                        context, ECommerceRoutes.userSettingsPage);
                  },
                ),
          ),
          ListTile( // have fixed-height, hence ideal for Menus
            leading: Icon(Icons.apps),
            title: Text("Catalog "),
            selected: _activeRouteBelowMenu == ECommerceRoutes.catalogPage,
            onTap: () => _navigate(ECommerceRoutes.catalogPage),
          ),
          ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text("Cart"),
            selected: _activeRouteBelowMenu == ECommerceRoutes.cartPage,
            onTap: () => _navigate(ECommerceRoutes.cartPage),
          ),
          ListTile(
            leading: Icon(Icons.person),
            title: Text("User Settings"),
            selected: _activeRouteBelowMenu == ECommerceRoutes.userSettingsPage,
            onTap: () => _navigate(ECommerceRoutes.userSettingsPage),
          ),
          AboutListTile( // A ListTile with onTap: showAboutDialog 2.Writing layout for the modal
            dense: true,
            icon: Icon(Icons.info),
            applicationName: "Grocery Store",
            applicationVersion: '1.0',
            applicationLegalese: 'All rigths reserved',
            aboutBoxChildren: <Widget>[
              Text("Developer contanct: satya.tryme@gmail.com"),
            ],
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: SatyaEmblem())

        ],
      ),
    );
  }
}

class SatyaEmblem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Text("\$atya",
          style: TextStyle(
            letterSpacing: 2.0,
            color: Colors.black45,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            shadows: [
              Shadow(
                  blurRadius: 3.0,
                  color: Colors.greenAccent,
                  offset: Offset(3.0, 3.0)),
              Shadow(
                  color: Colors.redAccent,
                  blurRadius: 3.0,
                  offset: Offset(-3.0, 3.0)),
            ],
          )),
    );
  }
}
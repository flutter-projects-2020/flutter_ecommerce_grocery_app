import 'dart:async';

import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/services/cart_service.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';
class AddToCartEvent {
  final Product product;final int qty;
  AddToCartEvent(this.product, this.qty);
}
class RemoveFromCartEvent {
  final String productTitle;final int qtyInCart;
  RemoveFromCartEvent(this.productTitle, this.qtyInCart);
}
// what if we use ChangeNotifierProvider method for outputs only
// on listening for new data from service , run (int v) {totalItemsInCart = v; notifyListeners();}
// instead of running a handler for adding that data to stream which the appBarCartIcon listens to
//
class CartBloc  {
  final CartService _cartService; // cartService connects this bloc & thus the whole app to Database/Firebase's Firestore

  // Inputs of the Bloc| StreamController/.sink will be exposed & StreamController.stream will be private
  // its StreamController/.sink will be added data/event from Widget by interaction from User
  // then that data comes on its corresponding stream which on listening adds data to Firebase's Firestore stream
  // Inputs | Event added by Widgets | those coming events listened & handled in this bloc-class
  // Add an item to the cart
  StreamController<AddToCartEvent> addToCartEventStreamCt = StreamController<AddToCartEvent>();
  // Remove an item from the cart
  StreamController<RemoveFromCartEvent> removeFromCartEventStreamCt = StreamController<RemoveFromCartEvent>();
  void startListeningForItemsChangeInCart() {
    // Listen to inputs from Widgets/User added to stream, then handle the data by the help of appropriate handler
    addToCartEventStreamCt.stream.listen(_handleAddItemsToCart);
    removeFromCartEventStreamCt.stream.listen(_handleRemoveCartItem);
  }

  // Input form User handler, adds an item to userCart in database
  void _handleAddItemsToCart(AddToCartEvent ae) async {
    // todo: In real-app this is the place to serialize an object JSON
    // todo : show progress indicator that item is getting added to cart
    await _cartService.addToCartInDatabase(ae.product, ae.qty);
    // todo: end progress indicator
  }

  void _handleRemoveCartItem(RemoveFromCartEvent re) {
    _cartService.removeFromCart(re.productTitle, re.qtyInCart);
  }

  // Outputs of the Bloc| StreamController/.sink will be private & StreamController.stream will be exposed
  // its StreamController/.sink will be added data/event on listening from a stream from Firebase's Firestore
  // Outputs | Listened by StreamBuilder widget| Data added in this bloc-class from an stream from Firebase's Firestore
  StreamController<Map<String, int>> _cartItemStreamCt = BehaviorSubject<Map<String, int>>.seeded({});
  // Get all items in the cart from the Database/Firebase's Firestore; if it has not loaded yet or there is no data, seeded value gets added to the stream immediately
  Stream<Map<String, int>> get cartItemsStream => _cartItemStreamCt.stream;

  StreamController<int> _cartItemCountStreamCt = BehaviorSubject<int>.seeded(0); // StreamController in rxdart referred to as Subject
  // Get number of items in the cart
  Stream<int> get cartItemCountStream => _cartItemCountStreamCt.stream;
  void startListeningForUpdatesFromDatabaseThroughService() {
    // listen for incoming data(from backend/database) to send as output(for widgets to render) | the data that you added as _addToCartEvent
    _cartService.cartCountStreamFromCartInDatabase.listen( // listening to backend, on an event/data from there it will be handled here
            (int count) {
          _cartItemCountStreamCt.add(count); // Added data from an External-source to the output-StreamController(whose stream that your UI will get its data from)
          // cartItemCount = count; notifyListeners();
        });
    _cartService.cartItemsMapStreamFromCartInDatabase.listen( // Anytime you get a new/updated value from the real-time database,
            (Map<String, int> data) => _cartItemStreamCt.add(data) // add/push that value to the output stream controller
    );
  }
  CartBloc(this._cartService) {
    startListeningForItemsChangeInCart(); //Widget -> Bloc -> Service -> Database
    startListeningForUpdatesFromDatabaseThroughService(); // Widget <- Bloc <- Service <- Database
  }


  close() {
    addToCartEventStreamCt.close();
    removeFromCartEventStreamCt.close();
    _cartItemStreamCt.close();
    _cartItemCountStreamCt.close();
  }
}
/*
class CartBloc with ChangeNotifier {
  final CartService _cartService; // cartService connects this bloc & thus the whole app to Database/Firebase's Firestore

  // Inputs of the Bloc| StreamController/.sink will be exposed & StreamController.stream will be private
  // its StreamController/.sink will be added data/event from Widget by interaction from User
  // then that data comes on its corresponding stream which on listening adds data to Firebase's Firestore stream
  // Inputs | Event added by Widgets | those coming events listened & handled in this bloc-class
  // Add an item to the cart
  StreamController<AddToCartEvent> addToCartEventStreamCt = StreamController<AddToCartEvent>();
  // Remove an item from the cart
  StreamController<RemoveFromCartEvent> removeFromCartEventStreamCt = StreamController<RemoveFromCartEvent>();

  // Input form User handler, adds an item to userCart in database
  void _handleAddItemsToCart(AddToCartEvent ae) async {
    // todo: In real-app this is the place to serialize an object JSON
    // todo : show progress indicator that item is getting added to cart
    await _cartService.addToCartInDatabase(ae.product, ae.qty);
    // todo: end progress indicator
  }

  void _handleRemoveCartItem(RemoveFromCartEvent re) {
    _cartService.removeFromCart(re.productTitle, re.qtyInCart);
  }

  // Outputs of the Bloc| ChangeNotifierProvider method used, on value of widget-depending variable changes, notifyListeners()
  // its value will be changed on listening from a stream from Firebase's Firestore
  Map<String, int> _cartItems = {}; // initializtion of variables for initial value, if initialization is too much complicated here, we can do it in the constructor programmatically
  Map<String, int> get cartItems => _cartItems;
  int _cartItemCount = 0;
  int get cartItemCount => _cartItemCount;

  CartBloc(this._cartService) {
    // Listen to inputs from Widgets/User added to stream, then handle the data by the help of appropriate handler
    addToCartEventStreamCt.stream.listen(_handleAddItemsToCart);
    removeFromCartEventStreamCt.stream.listen(_handleRemoveCartItem);

    // listen for incoming data(from backend/database) to send as output(for widgets to render) | the data that you added as _addToCartEvent
    _cartService.streamCartCountFromDatabase.listen( // listening to backend, on an event/data from there it will be handled here
            (int count) {
      _cartItemCount = count; // updated data from an External-source
      notifyListeners();
    });
    _cartService.streamCartItems.listen( // Anytime you get a new/updated value from the real-time database,
            (Map<String, int> data) { _cartItems = data; notifyListeners();}// assign that value to the output variable & notifyListeners()
    );
  }


  close() {
    addToCartEventStreamCt.close();
    removeFromCartEventStreamCt.close();
  }
}

class AddToCartEvent {
  final Product product;
  final int qty;

  AddToCartEvent(this.product, this.qty);
}

class RemoveFromCartEvent {
  final String productTitle;
  final int qtyInCart;

  RemoveFromCartEvent(this.productTitle, this.qtyInCart);
}

* */

/*
 * Copyright 2018 Eric Windmill. All rights reserved.
 * Use of this source code is governed by the MIT license that can be found in the LICENSE file.
 */

import 'package:e_commerce/blocs/cart_bloc.dart';
import 'package:e_commerce/blocs/catalog_bloc.dart';
import 'package:e_commerce/blocs/user_bloc.dart';
import 'package:e_commerce/services/cart_service.dart';
import 'package:e_commerce/services/catalog_service.dart';
import 'package:flutter/material.dart';
import 'package:e_commerce/main.dart';

//App

  // with the help of 'of' method you access an InheritedWidget, then you access its required property down in the tree
  /// returns State associated with this App
  /*static StatefulAppState of(BuildContext context) { // context is from the place you wanna access the inherited widget
    return (context.dependOnInheritedWidgetOfExactType<Provider>()).appState;
  }*/


// We can get a reference to this widget from anywhere down in the widget tree
// A Widget to go up in the tree & inject class-instances or data required across the underlying Widget tree
// This is immutable, that means its properties can't be reassigned, but the properties can change internally
class BlocsInjector extends InheritedWidget { // a Provider in itself, providing whatever you need down, in your tree
  // can access these members down in the tree, by accessing this class using the of method
  final AppState appState;
  final Blocs blocs;

 const BlocsInjector({
    Key key,
    @required this.appState,
    @required this.blocs,
    @required Widget child, // child parameter transferred to super InheritedWidget
  }) : super(key: key, child: child);

  // single required method on an InheritedWidget class
  // When an Inherited widget rebuilds, {the InheritedWidget will be rebuilt by the State in which it is in }
  // it may need to tell all widgets that depend on its data to rebuild as well
  // Old InheritedWidget given as argument, If this InheritedWidget rebuilt with the same data, then there's no need to make Flutter do the expensive work of rebuilding each context that depend on this InheritedWidget(context.dependOnInheritedWidgetOfExactType)
  @override
  bool updateShouldNotify(BlocsInjector oldWidget)
  => oldWidget.blocs != this.blocs; // todo: may be I can use this somehow to switch my app b/w different user, when one user log-outs & other log-ins | when user switches, appBlocs should also switch
  //oldWidget.appState != this.appState;
  // || oldWidget.appBlocs != this.appBlocs, no need to check for appBlocs too, because in this app I am passing the same appBlocs each time, the appState may change, which will affect the tree if it has a variable that changes over time

  // to make things simpler & readable, there is a tradition to include a 'of' method on InheritedWidget
  static BlocsInjector of(BuildContext context)
    => context.dependOnInheritedWidgetOfExactType<BlocsInjector>();
  //executing this, the framework registers this context as a dependent/Consumer of this InheritedWidget,
  //Once a widget registers a dependency on a particular type of InheritedWidget by calling this method, it will be rebuilt,
  //and [State.didChangeDependencies] will be called, whenever changes occur relating to that InheritedWidget
  //  Always place 'of' methods in your build(), as it fetches an InheritedWidget,
  //  placing here makes this context to get registered to rebuild when that InheritedWidget changes
  //  Usually, the InheritWidget doesn't changes because its marked immutable, but instances assigned to its field can change internally
}

class Services { // a provider of different types of services
  final CatalogService catalogService;
  final CartService cartService;

  Services({
    @required this.catalogService,
    @required this.cartService,
  });
}

//AppBloc
class Blocs { // a provider of different types of B.Lo.Cs
  CartBloc cartBloc;
  CatalogBloc catalogBloc;
  UserBloc userBloc;

  Blocs({
    @required this.cartBloc,
    @required this.catalogBloc,
    @required this.userBloc,
  });
}

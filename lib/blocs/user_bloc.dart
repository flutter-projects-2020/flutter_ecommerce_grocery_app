import 'dart:async';

import 'package:e_commerce/models/events/add_product.dart';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/models/user.dart';
import 'package:e_commerce/services/user_service.dart';
import 'package:rxdart/rxdart.dart';

// API provided by these classes to use data in them usually consist of simple
// No functions, No constants, No variables. 2 types of StreamController within B.Lo.Cs for
// Inputs(given to database) & Outputs(fetched from database) only
// A bloc can be middleman b/w services or backends or widgets(two widgets can use the same bloc to manage state, no need to use a StatefulWidget)
// 1 Bloc per Page | StreamControllers in it can handle coming-event(stream.listen(_handler)) or send-data(sink.add)
// each logical state-subject(list of Strings to produce Text widgets) has its own bloc
// Multiple widgets communicate with the same, few blocs that represent a logical piece of state
class UserBloc { // a method in this class can also be triggered by the backend(firebase) if this class is listening to it, which may result in rebuilding of a widget that's listening to this bloc/class
  final UserService _userService;
  //Sink to take data | Inputs send by components(widgets) as is, as there should not be business logic in the widget
  StreamController<UpdateUserEvent> updateUserInfoStreamController = StreamController<UpdateUserEvent>();
  StreamController<NewUserProductEvent> addNewProductToUserProductsStreamController =
      StreamController<NewUserProductEvent>(); // if sink not added, it will expose the whole StreamController to other classes, by adding sink, returns only sink view of the StreamController, so only sink related methods on this stream-controller gets exposed

  // Handler to manipulate data before outputting
  void _handleNewUserInformation(UpdateUserEvent event) {
    event.user.userProducts = [];
    _userService.updateUserInformation(event.user);
  }
  _handleNewUserProduct(NewUserProductEvent event) {
    var product = Product(
      category: event.product.category,
      title: event.product.title,
      cost: event.product.cost,
      imageTitle: ImageTitle.SlicedOranges,
    );
    _userService.addUserProduct(product);
  }

  // Stream to output data | Outputs should be ready to use by components(widgets)
  Stream<ECommerceUser> get user => _userStreamController.stream;
  StreamController _userStreamController = BehaviorSubject<ECommerceUser>.seeded(ECommerceUser(name: "Satya Prakash", contact: "satya.tryme@gmail.com"));

  UserBloc(this._userService) {
    updateUserInfoStreamController.stream.listen(_handleNewUserInformation);
    addNewProductToUserProductsStreamController.stream.listen(_handleNewUserProduct);
    _userService.streamUserInformation().listen((ECommerceUser data) {
      _userStreamController.add(data);
    });
  }


  close() {
    updateUserInfoStreamController.close();
    addNewProductToUserProductsStreamController.close();
    _userStreamController.close();
  }
}

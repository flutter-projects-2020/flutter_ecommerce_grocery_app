import 'dart:async';

import 'package:e_commerce/models/events/add_product.dart';
import 'package:e_commerce/models/product.dart';
import 'package:e_commerce/services/catalog_service.dart';
import 'package:e_commerce/utils/generate_cart_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

abstract class ProductEvent {
  NewProduct product;
  ProductEvent(this.product);
}
class AddProductEvent extends ProductEvent {
  AddProductEvent(NewProduct product) : super(product);
}

class UpdateProductEvent extends ProductEvent {
  UpdateProductEvent(NewProduct product) : super(product);
}

class CatalogBloc {
  final CatalogService _catalogService;

  // Inputs
  final _productInputController = StreamController<ProductEvent>.broadcast(); // can have more than 1 listener(=each listener executes a function when data gets added to this Subject/StreamController/Sink)
  Sink<ProductEvent> get addNewProduct => _productInputController.sink;
  Sink<ProductEvent> get updateProduct => _productInputController.sink;
  void startListeningForProductInput() {
    _productInputController.stream                                  // Stream<ProductEvent>
        .where((ProductEvent event) => event is UpdateProductEvent) // Stream<ProductEvent>
        .listen(_handleProductUpdate);
    _productInputController.stream
        .where((ProductEvent event) => event is AddProductEvent)
        .listen(_handleAddProduct);
  }

  _handleProductUpdate(ProductEvent event) {
    _catalogService.addNewProduct(
      Product(
        title: event.product.title,
        category: event.product.category,
        cost: event.product.cost,
        imageTitle: ImageTitle.SlicedOranges, // This is faked.
      ),
    );
  }
  _handleAddProduct(ProductEvent event) {
    _catalogService.addNewProduct(
      Product(
        category: event.product.category,
        title: event.product.title,
        cost: event.product.cost,
        imageTitle: ImageTitle.SlicedOranges,
      ),
    );
  }


  // Output of the Bloc: 2 ways :: 1. expose a stream 2. ChangeNotifierProvider method( I think this method will not work to build reactive catalog in app( an app that responds if data is available or not )
  ///  Steps to make a output stream :
  ///  1. make a controller
  ///  2. decide from where & 2.1 when data will be 2.3 added/pushed in its sink | data could coming from 1. another stream(like a database's stream) 2. or by running a method
  ///  3. expose its stream to listen to by Consumer/Widget for any data pushed/added
/*---------------------------------------------------------------------------------------------------------------------------------*/
  // streams list of all the product in catalog | need to stream part of this list a/q Category
  StreamController<List<Product>> _productListStreamController = BehaviorSubject<List<Product>>.seeded(buildInitialCatalog().availableProducts);
  void startListeningForUpdatedPlFromDbThroughService() {
    _catalogService.pLStream().listen((List<Product> pL) {
      _productListStreamController.add(pL); // when a new one is added, it will push out an updated list of products , its listeners will receive notifications & update themselves
    });
  }
  Stream<List<Product>> get productListStream => _productListStreamController.stream; //todo: Nowhere used
/*---------------------------------------------------------------------------------------------------------------------------------*/
  // break above all-product-Output to small grouped outputs a/q product-category | facilitates splitting catalog page by category | each category has its own header
  // solution: a new StreamController/.sink & stream for each ProductCategory
  List<StreamController<List<Product>>> _categoryPlCtrList = []; // List of (StreamControllers/.sink take pL)
  void populateCategoryPlStreamsList() {
    ProductCategory.values.forEach( (category) {
      var categoryPlCtr = BehaviorSubject<List<Product>>();                                 //-// 1.
      _catalogService.pLStreamFor(category).listen( (List<Product> pL) { categoryPlCtr.add(pL); } ); //-// 2.1 2.2 2.3
      categoryPlStreamsList.add(categoryPlCtr.stream);                                               //-// 3.
      _categoryPlCtrList.add(categoryPlCtr);
    }
    );
  }
  List<Stream<List<Product>>> categoryPlStreamsList = [];         // List of (StreamControllers.streams emit pL)
/*---------------------------------------------------------------------------------------------------------------------------------*/
  void populateCategoryPlStreamsListShortExplain() {
    ProductCategory.values.forEach( (category) { // code in this block, saves you from repeating the same code for each category
      var categoryPlCtr = BehaviorSubject<List<Product>>();    /// 1.  Created controller
      _catalogService.pLStreamFor(category)                             /// 2.1 From a stream from database through service
          .listen                                                       /// 2.2 When new data comes in that stream
        ( (pL) { categoryPlCtr.add(pL);                                 /// 2.3 Added/pushed that data
      } );
      categoryPlStreamsList.add(
          categoryPlCtr.stream                     /// 3. Made an stream out of that controller to be exposed
      );
      _categoryPlCtrList.add(categoryPlCtr); // controller are needed so they can be disposed, so its wise to keep hold onto them too
    }
    );
  }

  void populateCategoryPlStreamList() { // for that categoryPlStreamController needed
    /// Category Streams
    /// create a  stream controller and stream for each category
    ProductCategory.values.forEach( (category)
// ------- repeat code in order to create controllers for each category we have -------------
    { /// creating a list of controllers | Steps to make a output stream : make a controller , decide when data will be added/pushed in its sink, expose a stream to listen to it
      // 1. made controller
      var categoryPlCtr = BehaviorSubject<List<Product>>();//.seeded([]); // SCt for each 'category' to be added to the list<Sct> | seeded -> not works with seeded here, because the initial data is too complex
      // 2. decided when data will be added to it
      _catalogService.pLStreamFor(category).listen( // onData in this (category stream made from) all product stream from Database through Service
              (List<Product> pL) { categoryPlCtr.add(pL); /*categoryPl.add(pL);nL()*/}  // execute this
      );
      _categoryPlCtrList.add(categoryPlCtr); // we need the controller list to dispose them later

      categoryPlStreamsList.add(
        // 3. exposed it /// creating list of streams for each controller
          categoryPlCtr.stream
      );
    }
// ------- code ends ---------------------
    );

//    _categoryPlCtrList.forEach( (categoryPlCtr)
//        {
//            categoryPlStreamList.add(categoryPlCtr.stream);
//        }
//    );
  }

  CatalogBloc(this._catalogService) {
    startListeningForProductInput();
    startListeningForUpdatedPlFromDbThroughService();
    populateCategoryPlStreamsList();
  }


  void close() {
    _productListStreamController.close();
    _productInputController.close();
    _categoryPlCtrList.forEach((controller) => controller.close());
  }
}
// no need of StreamController & stream | when new data comes just update the Consumer/Widget depending property & call notifyListeners
//
//List<Map<ProductCategory, List<Product>>> _categoryPlList = []; // initializtion of the variable is too complex here, we can do that in the constructor programmatically
//In constructor ProductCategory.values.forEach( (category) { _categoryPlList.insert({category : []}) } )
//void populateCategoryPlList() { // we need to generated CategoryPl then add them to the list
//  ProductCategory.values.forEach( (category) {
//    _catalogService.pLStreamFor(category).listen( //we need to create a stream forEach ProductCategory & then listen to it | instead of repeating the same code we can do it in forEach
//            (List<Product> categoryPl) { _categoryPlList[category] = categoryPl; notifyListeners() } );  //-// 2.1 2.2 2.3reassign & nL()
//  }
//  );
//}
//List<Map<ProductCategory, List<Product>>> get categoryPlList = _categoryPlList;




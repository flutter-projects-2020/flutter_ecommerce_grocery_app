/*
 * Copyright 2018 Eric Windmill. All rights reserved.
 * Use of this source code is governed by the MIT license that can be found in the LICENSE file.
 */

import 'package:e_commerce/page/base/page_container.dart';
import 'package:e_commerce/utils/e_commerce_routes.dart';
import 'package:e_commerce/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ECommerceRoutes {
  static final catalogPage = '/'; // this will be initial route, because MaterialApp.initialRoute is by default set to '\'
  static final cartPage = '/cart';
  static final userSettingsPage = '/settings';
  static final cartItemDetailPage = '/itemDetail';
  static final addProductFormPage = '/addProduct';
}

//A NavigatorObserver that notifies RouteAware-s of activities that a Navigator does on/to a Route that it gives
//final RouteObserver<Route> routeObserver = RouteObserver<Route>();
//RouteObserver<R> informs subscribers whenever a route of type `R` is pushed
// on top of their own route of type `R` or popped from it. This is for example useful to keep track of page transitions
// To be informed about route changes of any type (R in this case) :
// 1. instantiate a RouteObserver<Route>() | routeObserver = RouteObserver<PageRoute>();
// 2. Register the RouteObserver as a navigation observer | MaterialApp(navigatorObservers: [routeObserver])
// 3. Make a Widget aware of its current [Route] state(ModalRoute.of(context), what routes are pushed above it or when its popped off
// 3.1 Implement RouteAware in a widget's state | class WidgetState extends State<Widget> with RouteAware {
// 3.2  subscribe it to the RouteObserver | routeObserver.subscribe(this, ModalRoute.of(context));
// Now State.didPush(when a route is pushed above ModalRoute.of(context)) & State.didPop(when current Route is popped off) methods will be get called

class ECommerceApp extends StatefulWidget {
  @override
  _ECommerceAppState createState() => _ECommerceAppState();
}

class _ECommerceAppState extends State<ECommerceApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

    /// All constants can be found in [utils/styles.dart]
    var _theme = ThemeData(
      // content
      backgroundColor: AppColors.background,
      textTheme: Theme.of(context).textTheme.apply(
            bodyColor: AppColors.textColor,
            displayColor: AppColors.textColor,
          ),
      // headings -- contrasts 'primary color'
      primaryTextTheme: Theme.of(context).textTheme.apply(
            bodyColor: AppColors.displayTextColor,
            displayColor: AppColors.displayTextColor,
          ),
      // ui -- contrasts 'accent color'
      accentTextTheme: Theme.of(context).textTheme.apply(
            bodyColor: AppColors.accentTextColor,
            displayColor: AppColors.accentTextColor,
          ),
      primaryColor: AppColors.primary,
      accentColor: AppColors.accent,
      primaryIconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: AppColors.displayTextColor),
      buttonColor: Colors.black54,
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: _theme,
      routes: { // ROUTE is the PATH to the SCREEN
        // A ROUTE
        ECommerceRoutes.catalogPage: (_) => // A PATH(NAME)
            Page(pageType: PageType.Catalog), // A SCREEN
        ECommerceRoutes.cartPage: (_) =>
            Page(pageType: PageType.Cart),
        ECommerceRoutes.userSettingsPage: (_) =>
            Page(pageType: PageType.Settings),
        ECommerceRoutes.addProductFormPage: (_) =>
            Page(pageType: PageType.AddProductForm),
      },
      //navigatorObservers: [routeObserver],
    );
  }
}
